<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,10) as $index) {
	        DB::table('company')->insert([
	            'companyname' => $faker->name,
	            'companyemail' => $faker->email,
	            'phone1' => $faker->e164PhoneNumber,
	            'phone2' => $faker->e164PhoneNumber,
	            'fax' => $faker->tollFreePhoneNumber,
	            'status' => 10,
	        ]);
		}
    }
}
