<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('username');
            $table->string('email', 100)->unique();
            $table->string('password');
            $table->string('identitynumber', 20)->nullable();
            $table->string('nationalidentity', 20)->nullable();
            $table->integer('status')->nullable();
            $table->integer('active')->default(1);
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->integer('classroom')->nullable();
            $table->date('birthdate')->nullable();
            $table->date('joindate')->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('mobilephone', 20)->nullable();
            $table->string('role', 20)->nullable();
            $table->integer('insertby')->default(1);
            $table->integer('updateby')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
