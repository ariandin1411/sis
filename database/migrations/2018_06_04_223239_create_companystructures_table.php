<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanystructuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companystructures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parentcompanyid');
            $table->integer('childcompanyid');
            $table->integer('active')->default(1);
            $table->integer('insertby')->default(1);
            $table->integer('updateby')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companystructures');
    }
}
