<?php

namespace App\Entities\Front\Company;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
	protected $table = 'company';
    protected $fillable = ['companyname', 'companyemail', 'website', 'phone1', 'phone2', 'phone3',
    						'fax', 'ext', 'status', 'parent', 'active', 'insertby', 'updateby',
    						'created_at', 'updated_at'];
}
