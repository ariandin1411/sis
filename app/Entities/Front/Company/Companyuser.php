<?php

namespace App\Entities\Front\Company;

use Illuminate\Database\Eloquent\Model;

class Companyuser extends Model
{
    protected $fillable = ['companyid', 'userid', 'active', 'insertby', 'updateby', 'created_at', 'updated_at'];
}
