<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Entities\Front\Company\Companyuser;
use App\Entities\Front\Company\Company;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        $company = Company::all();
        return view('auth.register', compact(['company']));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            //'name' => 'required|string|max:255',
            'username' => 'required|string|max:255|min:3|unique:users',
            'firstname' => 'required|string|max:255',
            'lastname' => 'string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'company' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'city' => 'string|max:255',
            'birthdate' => 'required|date',
            'mobilephone' => 'required|string|max:20',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        //dd($data);
        $user = User::create([
            'name' => $data['firstname'],
            'email' => $data['email'],
            'username' => $data['username'],
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'address' => $data['address'],
            'city' => $data['city'],
            'birthdate' => $data['birthdate'],
            'mobilephone' => $data['mobilephone'],
            'status' => 80,
            'role' => 'admin',
            'insertby' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'password' => bcrypt($data['password']),
        ]);

        Companyuser::create([
            'companyid' => $data['company'],
            'userid' => $user->id,
            'insertby' => $user->id,
            'created_at' => date('Y-m-d H:i:s'),
        ]);

        return $user;
    }
}
