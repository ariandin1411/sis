<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Front\Company\Company;

class DashboardController extends Controller
{
    public function admin()
    {
    	$com = Company::all();
    	return view('admin.dashboard', compact(['com']));
    }
}
